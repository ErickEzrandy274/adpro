abstract class Person {

    private String id = null;
    private String address = null;
    private String name = null;

    public Person(String id, String address, String name) {
        this.id = id;
        this.address = address;
        this.name = name;
    }

    public String getID() {
        return id;
    }

    public String getAddress() {
        return this.address;
    }

    public String getName() {
        return this.name;
    }

    public void setID(String id) {
        this.id = id;
    }

    public void setAddress(String addr) {
        this.address = addr;
    }

    public void setName(String name) {
        this.name = name;
    }

    abstract void study();

    abstract void work();
}
