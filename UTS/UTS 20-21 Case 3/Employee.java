class Employee extends Person {

    private double salary = 0.0;

    public Employee(String id, String name, String addr, double sal) {
        super(id, addr, name);
        this.salary = sal;
    }

    public String toString() {
        return (
            "ID: " + getID() +
            ", Name: " + getName() +
            ", Address: " + getAddress() +
            ", Salary: " + getSalary()
        );
    }

    public double getSalary() {
        if (isPalindrome(getName())) {
            return 1.1 * this.salary;
        } else {
            return this.salary;
        }
    }

    public void setSalary(double sal) {
        this.salary = sal;
    }

    public static boolean isPalindrome(String name) {
        int temp;
        if (name.length() % 2 == 0) {
            temp = name.length() / 2 - 1;
        } else {
            temp = (name.length() - 1) / 2 - 1;
        }

        for (int i = 0; i < temp; i++) {
            if (name.charAt(i) != name.charAt(name.length() - i - 1)) {
                return false;
            }
        }

        return true;
    }

    public void study() {
        System.out.println("Study the advanced technology for industry");
    }

    public void work() {
        System.out.println("Works on office tasks");
    }
}
