public class Demo {

    public static void main(String[] args) {
        Person person1 = new Employee("Kary101", "NABABAN", "Jakarta", 1000);
        Person person2 = new Student("Stud101", "Budihardjo", "Bandung", 3.01);
        System.out.println(person1.toString());

        person1.study();
        person1.work();
        System.out.println(person2.toString());

        person2.study();
        person2.work();

        person2 = new Employee("Kary102", "Budihardjo", "Bandung", 900);
        System.out.println(person2.toString());

        person2.study();
        person2.work();
    }
}
