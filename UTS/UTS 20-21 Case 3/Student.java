class Student extends Person {

    private double GPA = 0.0;

    public Student(String id, String name, String addr, double gpa) {
        super(id, addr, name);
        this.GPA = gpa;
    }

    public double getGPA() {
        return this.GPA;
    }

    public String toString() {
        return (
            "ID: " + getID() +
            ", Name: " + getName() +
            ", Address: " + getAddress() +
            ", GPA: " + getGPA()
        );
    }

    public void study() {
        System.out.println("Study basic science and technology");
    }

    public void work() {
        System.out.println("Works on campus assignments");
    }
}
