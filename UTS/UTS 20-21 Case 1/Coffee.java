package decorator;

class Espresso extends Coffee {

    public Espresso() {
        description = "Espresso";
    }

    @Override
    public double cost() {
        return 7.50;
    }
}

class NormalCoffee extends Coffee {

    public NormalCoffee() {
        description = "Normal Coffee";
    }

    @Override
    public double cost() {
        return 5.00;
    }
}