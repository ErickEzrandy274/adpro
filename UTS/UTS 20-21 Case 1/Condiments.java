package decorator;

abstract class CondimentDecorator extends Coffee {

    public abstract String getDescription();
}

class Milk extends CondimentDecorator{

    Coffee coffee;

    public Milk(Coffee coffee) {
        this.coffee = coffee;
    }

    @Override
    public double cost() {
        return this.coffee.cost() + 1.00;
    }
}

class Mocha extends CondimentDecorator {

    Coffee coffee;

    public Mocha(Coffee coffee) {
        this.coffee = coffee;
    }

    @Override
    public double cost() {
        return this.coffee.cost() + 1.50;
    }
}

class Whip extends CondimentDecorator {

    Coffee coffee;

    public Whip(Coffee coffee) {
        this.coffee = coffee;
    }

    @Override
    public double cost() {
        return this.coffee.cost() + 2.00;
    }
}