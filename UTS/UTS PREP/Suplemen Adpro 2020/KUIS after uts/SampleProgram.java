public class SampleProgram{

    boolean condition1(int a, int b) {
        return (a%b == 0);
    }


    boolean condition2(int a, int b) {
        
        return (b%a == 0);
    
    }

   int operation1(int a, int b) {
       return (a+b);
   }

   int operation2(int a, int b) {
       return (a-b);
   }

   int operation3(int a, int b) {
       return (a*b);

   }

   int thejob(int a, int b)  {
       // if b is a factor of a ,then add
       if (condition1(a,b)) return operation1(a,b);
       // if a is a factor of b, then substract
       else if (condition2(a,b)) return operation2(a,b);
       // if b is a factor of a  and a is a factor of b, then multiply
       else if (condition1(a,b) && condition2(a,b) ) return operation3(a,b);
       // else just return 0;
       return 0;
   }

}