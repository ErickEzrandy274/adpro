## BACA DULU GAN
1. Soal-soal disini adalah hasil tangkapan layar dari review ujian di scele
2. Jawaban yang tertera **BUKAN** merupakan solusi dari soal
3. Pada **UTS** selain soal teori ada pula soal programming melalui [Gitlab](https://gitlab.com/hafiyyan-teaching/midterm-2020-online/midterm-problem-set)
4. Pada **KUIS** & **UAS** file program yang terlampir adalah soal yang belum dilakukan pengeditan sama sekali
5. Gunakan file ini dengan sebaik baiknya yaaa

Resource tambahan: 

- [Refactoring Guru](https://refactoring.guru/)
- [Design Pattern Examples](https://gitlab.com/hafiyyan-teaching/designpatterns)

Sincerly,
*Arcturus*