package com.example.digitalsign.service;

public class SignatureService {
  
    // Spring Boot will read move.folder property from application.properties file and inject it
    // into moveFolder variable
    @Value(“${move.folder}”)
    private String moveFolder;
  
    public boolean isPDF(String fileName) {
      String[] parts = fileName.split(“.”);
      return parts[parts.length - 1].equalsIgnoreCase(“pdf”);
    }
  
    public boolean fileExists(String folderPath) {
      File folder = new File(moveFolder);
      
      if (!folder.canRead()) {
        folder.mkdir();
      }
  
      folder = new File(moveFolder);
  
      File[] listOfFiles = folder.listFiles(fileName -> isPDF(fileName));
      List<String> fileNames = new ArrayList<>();
  
    
  
      for (String fileName : fileNames) {
        if (fileName.equals(filename)) {
          return true;
        }
      }
  
      return false
    }
  }
