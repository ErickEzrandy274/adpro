class Stopwatch{

	private static Stopwatch stopwatch = null;

	private Stopwatch(){
		System.out.println("Stopwatch Initialized");
	}

	public static Stopwatch getStopwatchInstance(){
		if(stopwatch == null){
			stopwatch = new Stopwatch();
		}

		return stopwatch;
	}
}

class StopwatchSimulator implements Runnable{
   
   @Override
    public void run() {
    	Stopwatch.getStopwatchInstance();
    }
}

public class DigitalStopwatch{

	public static void main(String[] args){

		Runnable stopwatchRunnable = new StopwatchSimulator();
		new Thread(stopwatchRunnable, "Simulator 1").start();
		new Thread(stopwatchRunnable, "Simulator 2").start();
		new Thread(stopwatchRunnable, "Simulator 3").start();
	}

}