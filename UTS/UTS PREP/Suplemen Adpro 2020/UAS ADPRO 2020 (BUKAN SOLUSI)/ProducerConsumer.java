import java.util.*; 

public class ProducerConsumer implements Runnable {
	
   private Buffer buffer;  
   private int rounds;
   private boolean isproducer;
    ProducerConsumer(Buffer buff, int r, boolean flag) {
		this.buffer = buff;
		this.rounds = r;
		this.isproducer = flag;
    }
	
    public void run () {	
		for(int i = 0; i < rounds; i++) {
			if (isproducer) buffer.produce(i);
			else buffer.consume();
		 }
    }
    
    public static void main (String[] args) {		
		try {		
			Buffer buff = new Buffer();
			ProducerConsumer prod = new ProducerConsumer(buff, 5, true);
			ProducerConsumer cons= new ProducerConsumer(buff, 3, false);
			Thread[] producers = new Thread[4];
			Thread[] consumers = new Thread[4];
			
			for (int i =0; i < 4; i++) {
				producers[i] = new Thread (prod, "producer-"+i);
				consumers[i] = new Thread (cons, "consumer-"+i);
			}
			
			for (int i =0; i < 4; i++) {
				producers[i].start (); 
				consumers[i].start (); 				
			}		
			
			for (int i =0; i < 4; i++) {
				producers[i].join(); 
				consumers[i].join(); 				
			}		

			ArrayList<Integer> data = buff.getData();
			int sum = 0;
			for (int i =0;  i < data.size(); i++) {
				sum += data.get(i).intValue();
			}
			System.out.println("SUM : " + sum);
		} catch (Exception e) {
			System.out.println (e.getMessage());
		}
	}	
}