import java.util.*; 

public class Buffer {
	private ArrayList<Integer> data;
	Buffer(){
		this.data = new ArrayList<Integer>();
	}
	public void produce(int v) {
		this.data.add(new Integer(v));
	}
	public int consume() {
		 Integer v = this.data.remove(0);
		 return v.intValue();
	}
	public ArrayList<Integer> getData() {
		return this.data;
	}
}